#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

int numPhilosophers = 0;
pthread_t* philosophers;
int* forks;
char* states;
pthread_cond_t* conds;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *philosopher(void *i) {
    int index = *((int *)i);
	int left = index;
	int right = (index+1)%numPhilosophers;
	while(1) {
      //Thinking
      states[index] = 'T';
      sleep(rand()%10+1);
      //Hungry
      states[index] = 'H';
      
      pthread_mutex_lock(&mutex);
      if(index%2==0) {
        if(forks[left] == 1)
          pthread_cond_wait(conds+left, &mutex);
        (forks[left])++;
        if(forks[right] == 1)
          pthread_cond_wait(conds+right, &mutex);
        (forks[right])++;
      } else {
        if(forks[right] == 1)
          pthread_cond_wait(conds+right, &mutex);
        (forks[right])++; 
        if(forks[left] == 1)
          pthread_cond_wait(conds+left, &mutex);
        (forks[left])++;
      }
      pthread_mutex_unlock(&mutex);
      //Eating
      states[index] = 'E';
      sleep(rand()%10+1);
      
      pthread_mutex_lock(&mutex);
      (forks[left])--; (forks[right])--;
      pthread_cond_signal(conds+left);
      pthread_cond_signal(conds+right);
	  pthread_mutex_unlock(&mutex);
	}	
}

int main(int argc,char *argv[]) {
  if (argc > 1) {
  	numPhilosophers = atoi(argv[1]);
  } else {
    printf("ERRO: Especificar numero de filosofos\n");  
    return -1;  
  }
  philosophers  = malloc(sizeof(pthread_t)*numPhilosophers);
  forks         = malloc(sizeof(int)*numPhilosophers);
  states        = malloc(sizeof(char)*numPhilosophers);
  conds         = malloc(sizeof(pthread_cond_t)*numPhilosophers);
  
  int i;
  for (i=0; i< numPhilosophers; i++) {
    forks[i] = 0;
    pthread_cond_init(conds+i, NULL);
  }
  for (i=0; i< numPhilosophers; i++) {
    int *arg = malloc(sizeof(int));
	*arg = i;  
    pthread_create(&philosophers[i], NULL, philosopher, (void *)arg);
  }
  while (1) {
    sleep(1);
    for (i=0; i< numPhilosophers; i++) {
      printf("%c ", states[i]);
    }
    printf("\n");
  }  
  return 0;
}