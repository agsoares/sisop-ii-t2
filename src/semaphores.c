#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

int numPhilosophers = 0;
pthread_t* philosophers;
char* states;
sem_t* forks;
sem_t mutex;

void *philosopher(void *i) {
    int index = *((int *)i);
    int left = index;
	int right = (index+1)%numPhilosophers;
	while(1) {
      //Thinking
      states[index] = 'T';
      sleep(rand()%10+1);
      //Hungry
      states[index] = 'H';
      
      if(index%2==0){
        sem_wait(forks+left);
        sem_wait(forks+right);
      } else {
        sem_wait(forks+right);
        sem_wait(forks+left);
      }
      //Eating
      states[index] = 'E';
      sleep(rand()%10+1);
      
      sem_post(forks+left);
      sem_post(forks+right);
	}	
}

int main(int argc,char *argv[]) {
  if (argc > 1) {
  	numPhilosophers = atoi(argv[1]);
  } else {
    printf("ERRO: Especificar numero de filosofos\n");  
    return -1;  
  }
  philosophers  = malloc(sizeof(pthread_t)*numPhilosophers);
  states        = malloc(sizeof(char)*numPhilosophers);
  forks         = malloc(sizeof(sem_t)*numPhilosophers);
  
  sem_init(&mutex, 0, 1);
  
  int i;
  for (i=0; i< numPhilosophers; i++) {
    sem_init(forks+i, 0, 1);
  }
  for (i=0; i< numPhilosophers; i++) {
    int *arg = malloc(sizeof(int));
	*arg = i;  
    pthread_create(&philosophers[i], NULL, philosopher, (void *)arg);
  }
  while (1) {
    sleep(1);
    for (i=0; i< numPhilosophers; i++) {
      printf("%c ", states[i]);
    }
    printf("\n");
  }  
  return 0;
}
